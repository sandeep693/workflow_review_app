package com.ig.engage.workflow.model.dto;

import java.io.Serializable;
import java.util.List;

import com.ig.engage.workflow.model.Flow;
import com.ig.engage.workflow.model.Work;

public class WorkData implements Serializable {
	private static final long serialVersionUID = 1L;
	public Work work;
	public Long workId;
	public Long currentFlowId;
	public String currentFlowName;
	public Long prevFlowId;
	public String prevFlowName;
	public Long nextFlowId;
	public String nextFlowName;
	public List<Flow> flows;

	public WorkData() {
	}

	public WorkData(Work work, Long workId, List<Flow> flows, Long currentFlowId, String currentFlowName,
			Long prevFlowId, String prevFlowName, Long nextFlowId, String nextFlowName) {
		this.work = work;
		this.workId = workId;
		this.flows = flows;
		this.currentFlowId = currentFlowId;
		this.currentFlowName = currentFlowName;
		this.prevFlowId = prevFlowId;
		this.prevFlowName = prevFlowName;
		this.nextFlowId = nextFlowId;
		this.nextFlowName = nextFlowName;
	}

	public Work getWork() {
		return this.work;
	}

	public void setWork(Work work) {
		this.work = work;
	}

	public Long getWorkId() {
		return this.workId;
	}

	public void setWorkId(Long workId) {
		this.workId = workId;
	}

	public List<Flow> getFlows() {
		return this.flows;
	}

	public void setFlows(List<Flow> flows) {
		this.flows = flows;
	}

	public Long getCurrentFlowId() {
		return this.currentFlowId;
	}

	public void setCurrentFlowId(Long currentFlowId) {
		this.currentFlowId = currentFlowId;
	}

	public String getCurrentFlowName() {
		return this.currentFlowName;
	}

	public void setCurrentFlowName(String currentFlowName) {
		this.currentFlowName = currentFlowName;
	}

	public Long getPrevFlowId() {
		return this.prevFlowId;
	}

	public void setPrevFlowId(Long prevFlowId) {
		this.prevFlowId = prevFlowId;
	}

	public String getPrevFlowName() {
		return this.prevFlowName;
	}

	public void setPrevFlowName(String prevFlowName) {
		this.prevFlowName = prevFlowName;
	}

	public Long getNextFlowId() {
		return this.nextFlowId;
	}

	public void setNextFlowId(Long nextFlowId) {
		this.nextFlowId = nextFlowId;
	}

	public String getNextFlowName() {
		return this.nextFlowName;
	}

	public void setNextFlowName(String nextFlowName) {
		this.nextFlowName = nextFlowName;
	}
}
