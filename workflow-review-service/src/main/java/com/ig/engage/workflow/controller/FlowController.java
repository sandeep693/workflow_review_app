package com.ig.engage.workflow.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ig.engage.workflow.model.Flow;
import com.ig.engage.workflow.repository.service.FlowCrudRepositoryService;

@RestController
@RequestMapping({"/ig/engage/flow","/flow"})
public class FlowController
{
  @Autowired
  private FlowCrudRepositoryService fcrs;
  
  @RequestMapping(value={"/getflowbyflowid"}, method={RequestMethod.GET}, headers={"Accept=application/json"})
  public Flow getFlowById(@RequestParam Long flowId)
  {
    System.out.println("Finding flowId " + flowId);
    Flow obj = this.fcrs.findByFlowId(flowId);
    return obj;
  }
  
  @RequestMapping(value={"/getallflow"}, method={RequestMethod.GET}, headers={"Accept=application/json"})
  public List<Flow> getAllFlow()
  {
    List<Flow> list = this.fcrs.findAllFlow();
    return list;
  }
  
  @RequestMapping(value={"/getflowbyworkid"}, method={RequestMethod.GET}, headers={"Accept=application/json"})
  public List<Flow> getAllFlowByWorkId(@RequestParam Long workId)
  {
    List<Flow> list = this.fcrs.findFlowByWorkId(workId);
    return list;
  }
  
  @RequestMapping(value={"/getflowbyuserid"}, method={RequestMethod.GET}, headers={"Accept=application/json"})
  public List<Flow> getAllFlowByUserId(@RequestParam Long userId)
  {
    List<Flow> list = this.fcrs.findFlowByUserId(userId);
    return list;
  }
  
  @RequestMapping(value={"/addflow"}, method={RequestMethod.POST}, headers={"Accept=application/json"})
  public Flow addFlow(@RequestBody Flow flow)
  {
    return this.fcrs.addFlow(flow);
  }
  
  @RequestMapping(value={"/updateflow"}, method={RequestMethod.PUT}, headers={"Accept=application/json"})
  public Flow updateFlow(@RequestBody Flow flow)
  {
    return this.fcrs.updateflow(flow);
  }
  
  @RequestMapping(value={"/deleteflow"}, method={RequestMethod.DELETE}, headers={"Accept=application/json"})
  public void deleteFlow(@RequestParam Long flowId)
  {
    this.fcrs.deleteFlow(flowId);
  }
}
