package com.ig.engage.workflow.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "WORK")
public class Work implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "WORK_ID")
	public Long workId;
	@Column(name = "WORK_STATUS")
	public String workStatus;
	@Column(name = "NAME")
	public String name;
	@Column(name = "WORK_DESC")
	public String workDesc;
	@Column(name = "START_DATE")
	public Date startDate;
	@Column(name = "END_DATE")
	public Date endDate;
	@Column(name = "CURRENT_FLOW_NAME")
	public String currentFlowName;
	@Column(name = "CURRENT_FLOW_ID")
	public Long currentFlowId;
	@Column(name = "ASSIGNED_TO_USERID")
	public Long assignedToUserId;
	@Column(name = "ASSIGNED_TO_USERNAME")
	public String assignedToUserName;

	public Work() {
	}

	public Work(Long workId, String workStatus, String name, String workDesc, Date startDate, Date endDate,
			String currentFlowName, Long currentFlowId, Long assignedToUserId, String assignedToUserName) {
		super();
		this.workId = workId;
		this.workStatus = workStatus;
		this.name = name;
		this.workDesc = workDesc;
		this.startDate = startDate;
		this.endDate = endDate;
		this.currentFlowName = currentFlowName;
		this.currentFlowId = currentFlowId;
		this.assignedToUserId = assignedToUserId;
		this.assignedToUserName = assignedToUserName;
	}

	public Long getWorkId() {
		return workId;
	}

	public void setWorkId(Long workId) {
		this.workId = workId;
	}

	public String getWorkStatus() {
		return workStatus;
	}

	public void setWorkStatus(String workStatus) {
		this.workStatus = workStatus;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWorkDesc() {
		return workDesc;
	}

	public void setWorkDesc(String workDesc) {
		this.workDesc = workDesc;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCurrentFlowName() {
		return currentFlowName;
	}

	public void setCurrentFlowName(String currentFlowName) {
		this.currentFlowName = currentFlowName;
	}

	public Long getCurrentFlowId() {
		return currentFlowId;
	}

	public void setCurrentFlowId(Long currentFlowId) {
		this.currentFlowId = currentFlowId;
	}

	public Long getAssignedToUserId() {
		return assignedToUserId;
	}

	public void setAssignedToUserId(Long assignedToUserId) {
		this.assignedToUserId = assignedToUserId;
	}

	public String getAssignedToUserName() {
		return assignedToUserName;
	}

	public void setAssignedToUserName(String assignedToUserName) {
		this.assignedToUserName = assignedToUserName;
	}

}
