package com.ig.engage.workflow.repository.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ig.engage.workflow.model.Attachment;
import com.ig.engage.workflow.repository.AttachmentCrudRepository;

@Service
public class AttachmentCrudRepositoryService {
	
	@Autowired
	private AttachmentCrudRepository acr;
	
	public void addFile(Long attachmentId, byte[] bytes, String attachmentName) {
		Attachment attachment = new Attachment();
		attachment.setAttachmentId(attachmentId);
		attachment.setAttachmentFile(bytes);
		attachment.setAttachmentName(attachmentName);
		acr.save(attachment);
	}
	
	public Attachment findByAttachmentId(Long attachmentId) {
		return acr.findByAttachmentId(attachmentId);
	}
}
