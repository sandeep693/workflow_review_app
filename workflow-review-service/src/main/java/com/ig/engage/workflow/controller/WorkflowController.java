package com.ig.engage.workflow.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ig.engage.workflow.model.dto.WorkData;
import com.ig.engage.workflow.repository.service.WorkDataService;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping({ "/ig/engage","/igworkflow" })
public class WorkflowController {

	@Autowired
	private WorkDataService wds;

	@RequestMapping(value = { "/getworkdatabyuserid" }, method = { RequestMethod.GET }, headers = {"Accept=application/json" })
	public List<WorkData> getWorkByUserId(@RequestParam Long userId) {
		List<WorkData> list = wds.loadByUserId(userId);
		return list;
	}

	@RequestMapping(value = { "/movetonextflow" }, method = { RequestMethod.PUT }, headers = {"Accept=application/json" })
	public WorkData moveToNextFlow(@RequestParam Long workId, @RequestParam String comment) {
		WorkData obj = wds.onSuccessMoveToNextFlow(workId, comment);
		return obj;
	}

	@RequestMapping(value = { "/movetoprevflow" }, method = { RequestMethod.PUT }, headers = {"Accept=application/json" })
	public WorkData moveToPrevFlow(@RequestParam Long workId, @RequestParam String comment) {
		WorkData obj = wds.onFailureMoveToPrevFlow(workId, comment);
		return obj;
	}
	
	@RequestMapping(value = { "/clonebyworkid" }, method = { RequestMethod.PUT }, headers = {"Accept=application/json" })
	public WorkData cloneWork(@RequestParam Long workId) {
		WorkData obj = wds.cloneWork(workId);
		return obj;
	}
}
