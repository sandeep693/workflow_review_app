package com.ig.engage.workflow.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "FLOW_COMMENT")
@XmlRootElement
public class FlowComment implements Serializable {
	private static final long serialVersionUID = 1L;
	@XmlElement
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "COMMENT_ID")
	public Long commentId;
	@XmlElement
	@Column(name = "FLOW_ID")
	public Long flowId;
	@XmlElement
	@Column(name = "USER_ID")
	public Long userId;
	@XmlElement
	@Column(name = "USER_NAME")
	public String userName;
	@XmlElement
	@Column(name = "COMMENT_TEXT")
	public String commentText;
	@XmlElement
	@Column(name = "COMMENT_DATE")
	public Date commentDate;

	public FlowComment() {
	}

	public FlowComment(Long commentId, Long flowId, Long userId, String userName, String commentText,
			Date commentDate) {
		this.commentId = commentId;
		this.flowId = flowId;
		this.userId = userId;
		this.userName = userName;
		this.commentText = commentText;
		this.commentDate = commentDate;
	}

	public Long getCommentId() {
		return this.commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public Long getFlowId() {
		return this.flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCommentText() {
		return this.commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Date getCommentDate() {
		return this.commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}
}
