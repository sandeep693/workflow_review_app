package com.ig.engage.workflow.repository.service;

import com.ig.engage.workflow.model.Flow;
import com.ig.engage.workflow.model.FlowComment;
import com.ig.engage.workflow.model.Work;
import com.ig.engage.workflow.model.dto.WorkData;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorkDataService {
	@Autowired
	private WorkCrudRepositoryService wcrs;
	@Autowired
	private FlowCrudRepositoryService fcrs;
	@Autowired
	private FlowCommentCrudRepositoryService fccrs;

	public WorkData loadByWorkId(Long workId) {
		Work work = this.wcrs.findByWorkId(workId);
		List<Flow> flows = this.fcrs.findFlowByWorkId(workId);
		WorkData workData = loadWorkData(work, flows);
		return workData;
	}

	public List<WorkData> loadByUserId(Long userId) {
		List<WorkData> list = new ArrayList<WorkData>();
		Set<Work> works = this.wcrs.findbyAssigendToUserId(userId);
		if (works != null) {
			for (Work work : works) {
				WorkData workData = new WorkData();
				List<Flow> flows = this.fcrs.findFlowByWorkId(work.getWorkId());
				workData.setWork(work);
				workData.setFlows(flows);
				list.add(workData);
			}
		}
		return list;
	}

	public WorkData onSuccessMoveToNextFlow(Long workId, String comment) {
		Work work = this.wcrs.findByWorkId(workId);
		Flow nextFlow = null;
		Flow currentFlow = this.fcrs.findByFlowId(work.getCurrentFlowId());
		if ((currentFlow.getNextFlow() == null) || (currentFlow.getNextFlow().longValue() == 0L)) {
			nextFlow = this.fcrs.findByFlowId(currentFlow.getNextFlow());
			work.setCurrentFlowId(null);
			work.setCurrentFlowName(null);
			work.setAssignedToUserId(null);
			work.setAssignedToUserName(null);
			work.setWorkStatus("Close");
		} else {
			nextFlow = this.fcrs.findByFlowId(currentFlow.getNextFlow());
			work.setCurrentFlowId(nextFlow.getFlowId());
			work.setCurrentFlowName(nextFlow.getName());
			work.setAssignedToUserId(nextFlow.getUserId());
			work.setAssignedToUserName(nextFlow.getAssignTo());
		}
		this.wcrs.updateWork(work);

		currentFlow.setFlowStatus("Close");
		this.fcrs.updateflow(currentFlow);

		if ((currentFlow.getNextFlow() == null) || (currentFlow.getNextFlow().longValue() == 0L)) {
			this.fccrs.addFlowComment(currentFlow.getFlowId(), currentFlow.getUserId(), currentFlow.getAssignTo(), new Date(), comment);
		} else {
			this.fccrs.addFlowComment(nextFlow.getFlowId(), currentFlow.getUserId(), currentFlow.getAssignTo(), new Date(), comment);
		}

		return loadByWorkId(work.getWorkId());
	}

	public WorkData onFailureMoveToPrevFlow(Long workId, String comment) {
		Work work = this.wcrs.findByWorkId(workId);
		Flow currentFlow = this.fcrs.findByFlowId(work.getCurrentFlowId());
		Flow prevFlow = null;
		if ((currentFlow.getPrevFlow() == null) || (currentFlow.getPrevFlow().longValue() == 0L)) {
			return null;
		}
		prevFlow = this.fcrs.findByFlowId(currentFlow.getPrevFlow());
		work.setCurrentFlowId(prevFlow.getFlowId());
		work.setCurrentFlowName(prevFlow.getName());
		work.setAssignedToUserId(prevFlow.getUserId());
		work.setAssignedToUserName(prevFlow.getAssignTo());
		this.wcrs.updateWork(work);

		currentFlow.setFlowStatus("Open");
		this.fcrs.updateflow(currentFlow);
		this.fccrs.addFlowComment(prevFlow.getFlowId(), currentFlow.getUserId(), currentFlow.getAssignTo(), new Date(),
				comment);

		return loadByWorkId(work.getWorkId());
	}

	public WorkData loadWorkData(Work work, List<Flow> flows) {
		WorkData workData = new WorkData();
		Flow currentFlow = this.fcrs.findByFlowId(work.getCurrentFlowId());
		Flow nextFlow = new Flow();
		Flow prevFlow = new Flow();
		Long nextFlowId = null;
		String nextFlowName = null;
		try {
			if ((currentFlow.getNextFlow() == null) || (currentFlow.getNextFlow().longValue() == 0L)) {
				nextFlowId = null;
				nextFlowName = null;
			} else {
				nextFlow = this.fcrs.findByFlowId(currentFlow.getNextFlow());
				if (nextFlow != null) {
					nextFlowId = nextFlow.getFlowId();
					nextFlowName = nextFlow.getName();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Long prevFlowId = null;
		String prevFlowName = null;
		try {
			if ((currentFlow.getPrevFlow() == null) || (currentFlow.getPrevFlow().longValue() == 0L)) {
				prevFlowId = null;
				prevFlowName = null;
			} else {
				prevFlow = this.fcrs.findByFlowId(currentFlow.getPrevFlow());
				if (prevFlow != null) {
					prevFlowId = prevFlow.getFlowId();
					prevFlowName = prevFlow.getName();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		workData.setWork(work);
		workData.setFlows(flows);
		if(currentFlow == null) {
			workData.setCurrentFlowId(null);
			workData.setCurrentFlowName(null);
		} else {
			workData.setCurrentFlowId(currentFlow.getFlowId());
			workData.setCurrentFlowName(currentFlow.getName());
		}
		if (nextFlow.getFlowId() != null) {
			workData.setNextFlowId(nextFlowId);
			workData.setNextFlowName(nextFlowName);
		}
		if (prevFlow.getFlowId() != null) {
			workData.setPrevFlowId(prevFlowId);
			workData.setPrevFlowName(prevFlowName);
		}
		workData.setWorkId(work.getWorkId());
		return workData;
	}
	
	
	public WorkData cloneWork(Long workId) {
		Long newWorkId = 1L;
		Long flowId = 1L;
		List<Work> workList = wcrs.findAllWork();
		for(Work o: workList) {
			if(o.getWorkId().longValue() > newWorkId.longValue()) {
				newWorkId = o.getWorkId();
			}
		}
		newWorkId = new Long(newWorkId.longValue() + 1);
		
		List<Flow> flowList = fcrs.findAllFlow();
		for(Flow o: flowList) {
			if(o.getFlowId().longValue() > flowId.longValue()) {
				flowId = o.getFlowId();
			}
		}
		Long newFlowId =  new Long(flowId.longValue() + 1);
		
		
		Work oldWork = wcrs.findByWorkId(workId);
		List<Flow> oldFlowList = fcrs.findFlowByWorkId(workId);
		
		List<Flow> cloneFlowList = new ArrayList<Flow>();
		// add flow Clones
		for(Flow o: oldFlowList) {
			Flow flowClone = getFlow(o, newFlowId);
			flowClone = fcrs.addFlow(flowClone);
			System.out.println("created clone Flow - ID : "+flowClone.getFlowId()+", Flow Name : "+flowClone.getName());
			newFlowId = new Long(flowClone.getFlowId().longValue() + 1);
			cloneFlowList.add(flowClone);
		}
		
		
		//add clone work
		Work cloneWork = getWork(oldWork, newWorkId);
		cloneWork = wcrs.addWork(cloneWork);
		System.out.println("cloneWork created - Work ID : "+cloneWork.getWorkId());
		
		// update clone work
		cloneWork.setAssignedToUserId(cloneFlowList.get(0).getUserId());
		cloneWork.setAssignedToUserName(cloneFlowList.get(0).getAssignTo());
		cloneWork.setCurrentFlowId(cloneFlowList.get(0).getFlowId());
		cloneWork.setCurrentFlowName(cloneFlowList.get(0).getName());
		cloneWork.setWorkStatus("Open");
		wcrs.updateWork(cloneWork);
		System.out.println("cloneWork updated");		
		
		//update flow clones
		for(Flow o: cloneFlowList) {
			o.setFlowCommentSet(new HashSet<FlowComment>());
			o.setFlowStatus("Open");
			if(o.getNextFlow() != null) {
				int index = cloneFlowList.indexOf(o);
				o.setNextFlow(cloneFlowList.get(index + 1).getFlowId());
			}
			
			if(o.getPrevFlow() != null) {
				int index = cloneFlowList.indexOf(o);
				o.setPrevFlow(cloneFlowList.get(index - 1).getFlowId());
			}
			o.setWorkId(cloneWork.getWorkId());
			fcrs.updateflow(o);
			System.out.println("flow updated");
		}
		
		WorkData workData = loadWorkDataByWorkId(cloneWork.getWorkId());
		
		return workData;
	}
	
	public WorkData loadWorkDataByWorkId(Long workId) {
		Work work = wcrs.findByWorkId(workId);
		WorkData workData = new WorkData();
		List<Flow> flows = fcrs.findFlowByWorkId(work.getWorkId());
		workData.setWork(work);
		workData.setFlows(flows);
		return workData;
	}	
	
	public Work getWork(Work work, Long workId) {
		Work obj = new Work();
		obj.setWorkId(workId);
		obj.setName(work.getName());
		obj.setStartDate(work.getStartDate());
		obj.setEndDate(work.getEndDate());
		obj.setWorkDesc(work.getWorkDesc());
		return obj;
	}
	
	public Flow getFlow(Flow flow, Long flowId) {
		Flow obj = new Flow();
		obj.setFlowId(flowId);
		obj.setAssignTo(flow.getAssignTo());
		obj.setEndDate(flow.getEndDate());
		obj.setStartDate(flow.getStartDate());
		obj.setFlowDesc(flow.getFlowDesc());
		obj.setName(flow.getName());
		obj.setFlowStatus(flow.getFlowStatus());
		obj.setNextFlow(flow.getNextFlow());
		obj.setNextFlowLabel(flow.getNextFlowLabel());
		obj.setPrevFlow(flow.getPrevFlow());
		obj.setPrevFlowLabel(flow.getPrevFlowLabel());
		obj.setUserId(flow.getUserId());
		return obj;
	}
	
}
