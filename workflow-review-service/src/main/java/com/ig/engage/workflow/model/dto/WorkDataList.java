package com.ig.engage.workflow.model.dto;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WorkDataList {
	@XmlElement
	List<WorkData> workDataList;

	public WorkDataList(List<WorkData> workDataList) {
		this.workDataList = workDataList;
	}

	public WorkDataList() {
	}

	public List<WorkData> getWorkDataList() {
		return this.workDataList;
	}

	public void setWorkDataList(List<WorkData> workDataList) {
		this.workDataList = workDataList;
	}
}
