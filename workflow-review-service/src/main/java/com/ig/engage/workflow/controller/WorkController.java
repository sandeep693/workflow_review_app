package com.ig.engage.workflow.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ig.engage.workflow.model.Work;
import com.ig.engage.workflow.repository.service.WorkCrudRepositoryService;

@RestController
@RequestMapping({ "/ig/engage/work","/work" })
public class WorkController {
	@Autowired
	private WorkCrudRepositoryService wcrs;

	@RequestMapping(value = { "/getworkbyworkid" }, method = { RequestMethod.GET }, headers = {
			"Accept=application/json" })
	public Work getWorkById(@RequestParam Long workId) {
		System.out.println("Finding workId " + workId);
		Work obj = this.wcrs.findByWorkId(workId);
		return obj;
	}

	@RequestMapping(value = { "/getallwork" }, method = { RequestMethod.GET }, headers = { "Accept=application/json" })
	public List<Work> getAllWork() {
		List<Work> list = this.wcrs.findAllWork();
		return list;
	}

	@RequestMapping(value = { "/addwork" }, method = { RequestMethod.POST }, headers = { "Accept=application/json" })
	public Work addWork(@RequestBody Work work) {
		return this.wcrs.addWork(work);
	}

	@RequestMapping(value = { "/updatework" }, method = { RequestMethod.PUT }, headers = { "Accept=application/json" })
	public Work updateWork(@RequestBody Work work) {
		return this.wcrs.updateWork(work);
	}

	@RequestMapping(value = { "/deletework" }, method = { RequestMethod.DELETE }, headers = {
			"Accept=application/json" })
	public void deleteWork(@RequestParam Long workId) {
		this.wcrs.deleteWork(workId);
	}
}
