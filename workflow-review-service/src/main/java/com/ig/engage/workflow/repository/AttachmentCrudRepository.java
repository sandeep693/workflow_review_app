package com.ig.engage.workflow.repository;

import org.springframework.data.repository.CrudRepository;

import com.ig.engage.workflow.model.Attachment;

public interface AttachmentCrudRepository extends CrudRepository<Attachment, Long>{

	Attachment findByAttachmentId(Long attachmentId);
	
}
