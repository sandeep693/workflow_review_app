package com.ig.engage.workflow.repository.service;

import com.ig.engage.workflow.model.Flow;
import com.ig.engage.workflow.repository.FlowCrudRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FlowCrudRepositoryService {
	@Autowired
	private FlowCrudRepository fcr;

	public Flow addFlow(Flow flow) {
		return  fcr.save(flow);
	}

	public String deleteFlow(Long flowId) {
		fcr.delete(flowId);
		return "Flow deleted flowId - "+flowId;
	}

	public Flow findByFlowId(Long flowId) {
		return this.fcr.findByFlowId(flowId);
	}

	public List<Flow> findAllFlow() {
		List<Flow> flowList = new ArrayList<Flow>();
		for (Flow o : this.fcr.findAll()) {
			flowList.add(o);
		}
		return flowList;
	}

	public List<Flow> findFlowByWorkId(Long workId) {
		return this.fcr.findByWorkId(workId);
	}

	public List<Flow> findFlowByAssignTo(Long assignTo) {
		List<Flow> flowList = new ArrayList<Flow>();
		for (Flow o : this.fcr.findByUserId(assignTo)) {
			flowList.add(o);
		}
		return flowList;
	}

	public List<Flow> findFlowByUserId(Long userId) {
		List<Flow> flowList = new ArrayList<Flow>();
		for (Flow o : this.fcr.findByUserId(userId)) {
			flowList.add(o);
		}
		return flowList;
	}

	public Flow updateflow(Flow flow) {
		Flow obj = (Flow) this.fcr.findOne(flow.getFlowId());
		obj.setWorkId(flow.getWorkId());
		obj.setName(flow.getName());
		obj.setFlowDesc(flow.getFlowDesc());
		obj.setStartDate(flow.getStartDate());
		obj.setEndDate(flow.getEndDate());
		obj.setNextFlow(flow.getNextFlow());
		obj.setPrevFlow(flow.getPrevFlow());
		obj.setFlowStatus(flow.getFlowStatus());

		return this.fcr.save(obj);
	}
}
