package com.ig.engage.workflow.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ig.engage.workflow.model.FlowComment;


public interface FlowCommentCrudRepository extends CrudRepository<FlowComment, Long>{

	FlowComment findByCommentId(Long commentId);
	List<FlowComment> findByFlowId(Long flowId);
	
	@Query(value="SELECT MAX(COMMENT_ID) FROM FLOW_COMMENT", nativeQuery=true)
	Long findMaxId();
	
}
