package com.ig.engage.workflow.repository;

import com.ig.engage.workflow.model.Work;
import java.util.Set;
import org.springframework.data.repository.CrudRepository;

public interface WorkCrudRepository extends CrudRepository<Work, Long> {
	
	
	public abstract Work findByWorkId(Long paramLong);

	public abstract Set<Work> findByAssignedToUserId(Long paramLong);
}
