package com.ig.engage.workflow.model;

import java.io.Serializable;
import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@javax.persistence.Entity
@Table(name = "WORK_ATTACHMENT")
@XmlRootElement
public class Attachment implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@XmlElement
	@Id
	@Column(name = "ATTCHMENT_ID")
	public Long attachmentId;
	
	@XmlElement
	@Column(name = "ATTACHMENT_FILE")
	public byte[] attachmentFile;
	
	@XmlElement
	@Column(name = "ATTACHMENT_NAME")
	public String attachmentName;

	public Attachment() {
		super();
	}

	public Attachment(Long attachmentId, byte[] attachmentFile, String attachmentName) {
		super();
		this.attachmentId = attachmentId;
		this.attachmentFile = attachmentFile;
		this.attachmentName = attachmentName;
	}

	public Long getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(Long attachmentId) {
		this.attachmentId = attachmentId;
	}

	public byte[] getAttachmentFile() {
		return attachmentFile;
	}

	public void setAttachmentFile(byte[] attachmentFile) {
		this.attachmentFile = attachmentFile;
	}

	public String getAttachmentName() {
		return attachmentName;
	}

	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}



}
