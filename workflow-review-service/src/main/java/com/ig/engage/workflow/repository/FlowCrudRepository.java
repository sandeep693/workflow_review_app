package com.ig.engage.workflow.repository;

import com.ig.engage.workflow.model.Flow;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public abstract interface FlowCrudRepository extends CrudRepository<Flow, Long> {
	public abstract Flow findByFlowId(Long paramLong);

	public abstract List<Flow> findByWorkId(Long paramLong);

	public abstract List<Flow> findByUserId(Long paramLong);
}
