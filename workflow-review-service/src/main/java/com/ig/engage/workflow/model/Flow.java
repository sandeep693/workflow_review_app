package com.ig.engage.workflow.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "FLOW")
public class Flow implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "FLOW_ID")
	public Long flowId;
	@Column(name = "WORK_ID")
	public Long workId;
	@Column(name = "NAME")
	public String name;
	@Column(name = "FLOW_DESC")
	public String flowDesc;
	@Column(name = "START_DATE")
	public Date startDate;
	@Column(name = "END_DATE")
	public Date endDate;
	@Column(name = "NEXT_FLOW")
	public Long nextFlow;
	@Column(name = "PREV_FLOW")
	public Long prevFlow;
	@Column(name = "FLOW_STATUS")
	public String flowStatus;
	@Column(name = "ASSIGN_TO")
	public String assignTo;
	@Column(name = "USER_ID")
	public Long userId;
	@Column(name = "NEXT_FLOW_LABEL")
	public String nextFlowLabel;
	@Column(name = "PREV_FLOW_LABEL")
	public String prevFlowLabel;
	@OneToMany(mappedBy = "flowId", fetch = FetchType.EAGER)
	public Set<FlowComment> flowCommentSet;

	public Flow() {
	}

	public Flow(Long flowId, Long workId, String name, String flowDesc, Date startDate, Date endDate, Long nextFlow,
			Long prevFlow, String flowStatus, String assignTo, Long userId, Set<FlowComment> flowCommentSet) {
		this.flowId = flowId;
		this.workId = workId;
		this.name = name;
		this.flowDesc = flowDesc;
		this.startDate = startDate;
		this.endDate = endDate;
		this.nextFlow = nextFlow;
		this.prevFlow = prevFlow;
		this.flowStatus = flowStatus;
		this.assignTo = assignTo;
		this.userId = userId;
		this.flowCommentSet = flowCommentSet;
	}

	public String getNextFlowLabel() {
		return this.nextFlowLabel;
	}

	public void setNextFlowLabel(String nextFlowLabel) {
		this.nextFlowLabel = nextFlowLabel;
	}

	public String getPrevFlowLabel() {
		return this.prevFlowLabel;
	}

	public void setPrevFlowLabel(String prevFlowLabel) {
		this.prevFlowLabel = prevFlowLabel;
	}

	public Long getFlowId() {
		return this.flowId;
	}

	public void setFlowId(Long flowId) {
		this.flowId = flowId;
	}

	public Long getWorkId() {
		return this.workId;
	}

	public void setWorkId(Long workId) {
		this.workId = workId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFlowDesc() {
		return this.flowDesc;
	}

	public void setFlowDesc(String flowDesc) {
		this.flowDesc = flowDesc;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Long getNextFlow() {
		return this.nextFlow;
	}

	public void setNextFlow(Long nextFlow) {
		this.nextFlow = nextFlow;
	}

	public Long getPrevFlow() {
		return this.prevFlow;
	}

	public void setPrevFlow(Long prevFlow) {
		this.prevFlow = prevFlow;
	}

	public String getFlowStatus() {
		return this.flowStatus;
	}

	public void setFlowStatus(String flowStatus) {
		this.flowStatus = flowStatus;
	}

	public String getAssignTo() {
		return this.assignTo;
	}

	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Set<FlowComment> getFlowCommentSet() {
		return this.flowCommentSet;
	}

	public void setFlowCommentSet(Set<FlowComment> flowCommentSet) {
		this.flowCommentSet = flowCommentSet;
	}
}
