package com.ig.engage.workflow.repository.service;

import com.ig.engage.workflow.model.Work;
import com.ig.engage.workflow.repository.WorkCrudRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorkCrudRepositoryService {
	@Autowired
	private WorkCrudRepository workCrudRepository;

	public Work addWork(Work work) {
		return  workCrudRepository.save(work);
	}

	public String deleteWork(Long workId) {
		this.workCrudRepository.delete(workId);
		return "Work deleted. WorkID : "+ workId;
	}

	public Work updateWork(Work work) {
		Work obj = workCrudRepository.findOne(work.getWorkId());
		
		if(obj==null){
			return null;
		}
		
		obj.setStartDate(work.getStartDate());
		obj.setEndDate(work.getEndDate());
		obj.setCurrentFlowId(work.getCurrentFlowId());
		obj.setCurrentFlowName(work.getCurrentFlowName());
		obj.setName(work.getName());
		obj.setWorkDesc(work.getWorkDesc());
		obj.setWorkStatus(work.getWorkStatus());

		return workCrudRepository.save(obj);
	}

	public Work findByWorkId(Long workId) {
		return this.workCrudRepository.findByWorkId(workId);
	}

	public Set<Work> findbyAssigendToUserId(Long userId) {
		return this.workCrudRepository.findByAssignedToUserId(userId);
	}

	public List<Work> findAllWork() {
		
		return (List<Work>) workCrudRepository.findAll();
		
		
		
		//return workList;
	}
}
