package com.ig.engage.workflow.repository.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ig.engage.workflow.model.FlowComment;
import com.ig.engage.workflow.repository.FlowCommentCrudRepository;

@Service
public class FlowCommentCrudRepositoryService {
	@Autowired
	private FlowCommentCrudRepository fccr;

	public List<FlowComment> findByFlowId(Long flowId) {
		return this.fccr.findByFlowId(flowId);
	}

	public void addFlowComment(Long flowId, Long userId, String userName, Date commentDate, String commentText) {
		FlowComment obj = new FlowComment();
		//Long commentId = findMaxId();
		//System.out.println("commentId - " + commentId);
		//obj.setCommentId(commentId);
		obj.setFlowId(flowId);
		obj.setCommentDate(commentDate);
		obj.setCommentText(commentText);
		obj.setUserId(userId);
		obj.setUserName(userName);
		this.fccr.save(obj);
	}

	public Long findMaxId() {
		Long maxId = new Long(1);
		try {
			maxId = this.fccr.findMaxId();
			maxId = new Long(1L + maxId.longValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return maxId;
	}
}
