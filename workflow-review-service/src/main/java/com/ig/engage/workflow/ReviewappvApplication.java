package com.ig.engage.workflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


@EnableSwagger2
@SpringBootApplication
//@PropertySource("${WORKFLOW_DEFAULT_CONFIG}")
public class ReviewappvApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReviewappvApplication.class, args);
	}
}
