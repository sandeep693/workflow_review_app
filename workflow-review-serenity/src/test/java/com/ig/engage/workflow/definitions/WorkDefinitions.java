package com.ig.engage.workflow.definitions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import net.thucydides.core.annotations.Steps;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.ig.engage.workflow.steps.WorkSteps;


public class WorkDefinitions {

	
	@Steps
	private WorkSteps apiSteps;
	
	@When("I call getAll WORK from WorkFlow All api")
	public void callForWorkFlowApi(){
		apiSteps.requestWork();
	}
	
	 @Then("I should getAll WORK response status code $expectedStatusCode")
	public void thenResponseSuccess(int expectedStatusCode){
		 assertThat("Wrong response status code", apiSteps.getWork() ,equalTo(expectedStatusCode));
	}
	
	 @When("I call ADD WORKFLOW WorkFlow All api")
	public void callForAddWork(){
		apiSteps.requestAddWork();
	}
	@Then("I should ADD WORK response status code $expectedStatusCode")
	public void thenAddResponseSuccess(int expectedStatusCode){
		assertThat("Wrong response status code", apiSteps.responseAddWork() ,equalTo(expectedStatusCode));
	}
	
	@When("I call UPDATE WorkFlow All api")
	public void callForUpdateWork(){
		apiSteps.requestUpdateWork();
	}
	@Then("I should UPDATE response status code $expectedStatusCode")
	public void thenUpdateResponseSuccess(int expectedStatusCode){
		assertThat("Wrong response status code", apiSteps.responseUpdateWork() ,equalTo(expectedStatusCode));
	}
	
	@When("I call DELETE WorkFlow All api")
	public void callForDeleteWork(){
		apiSteps.requestDeleteWork();
	}
	
	@Then("I should DELETE response status code $expectedStatusCode")
	public void thenDeleteResponseWork(int expectedStatusCode){
		assertThat("Wrong response status code", apiSteps.responseDeleteWork() ,equalTo(expectedStatusCode));
	}
	
	
	
}
