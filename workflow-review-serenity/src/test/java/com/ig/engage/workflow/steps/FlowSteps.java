package com.ig.engage.workflow.steps;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class FlowSteps {

	public static int flowId = 0;

	String baseUrl;
	
	public FlowSteps() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Step
	public void requestAddFlow() {

		Map<String, Object> addMap = new HashMap<String, Object>();

		addMap.put("assignTo", "string");
		addMap.put("nextFlow", 0);
		addMap.put("currentFlowId", 0);
		addMap.put("currentFlowName", "String");
		addMap.put("endDate", new Date());
		addMap.put("name", "SerenityFlowTest");
		addMap.put("startDate", new Date());
		addMap.put("flowDesc", "SerenityFlowTest");
		addMap.put("flowStatus", "String");
		addMap.put("userId", 0);
		addMap.put("workId", 0);

		flowId = SerenityRest.given().contentType("application/json")
				.content(addMap).log().body().when()
				.post("http://localhost:8484/workflow/ig/engage/flow/addflow")
				.then().extract().body().jsonPath().get("flowId");
		
	}

	@Step
	public int responseAddFlow() {
		return SerenityRest.then().extract().statusCode();
	}

	@Step
	public void requestGetFlow() {
		SerenityRest
				.given()
				.contentType("application/json")
				.when()
				.get("http://localhost:8484/workflow/ig/engage/flow/getallflow");
	}

	@Step
	public int responseGetFlow() {
		return SerenityRest.then().extract().statusCode();
	}

	@Step
	public void requestUpdateFlow() {
		Map<String, Object> addMap = new HashMap<String, Object>();

		
		addMap.put("flowId", flowId);
		addMap.put("assignTo", "string");
		addMap.put("nextFlow", 0);
		addMap.put("currentFlowId", 0);
		addMap.put("currentFlowName", "String");
		addMap.put("endDate", new Date());
		addMap.put("name", "SerenityFlowTest");
		addMap.put("startDate", new Date());
		addMap.put("flowDesc", "SerenityFlowTest");
		addMap.put("flowStatus", "String");
		addMap.put("userId", 0);
		addMap.put("workId", 0);

		flowId = SerenityRest.given().contentType("application/json")
				.content(addMap).log().body().when()
				.put("http://localhost:8484/workflow/ig/engage/flow/updateflow")
				.then().extract().body().jsonPath().get("flowId");
	}

	@Step
	public int responseUpdateFlow() {
		return SerenityRest.then().extract().statusCode();
	}

	@Step
	public void requestDeleteFlow() {
		SerenityRest
		.given()
		.contentType("application/json")
		.when()
		.delete("http://localhost:8484/workflow/ig/engage/flow/deleteflow?flowId="+flowId);
	}

	@Step
	public int responseDeleteFlow() {
		return SerenityRest.then().extract().statusCode();
	}

	public void requestGetFlowByFlowId(int flowId) {
		SerenityRest
				.given()
				.contentType("application/json")
				.when()
				.get("http://localhost:8484/workflow/ig/engage/flow/getflowbyflowid?flowId="
						+ flowId);
	}

	public int responseGetFlowByFlowId() {
		return SerenityRest.then().extract().statusCode();
	}
}
