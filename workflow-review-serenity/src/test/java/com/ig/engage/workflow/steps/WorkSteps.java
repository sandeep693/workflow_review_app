package com.ig.engage.workflow.steps;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class WorkSteps {
	
	String baseUrl;
	
	public WorkSteps() {
		super();
		// TODO Auto-generated constructor stub
		this.baseUrl = System.getProperty("running.app.baseurl");
	}
	
	public static int workId=0;
	
	@Step
	public void requestWork() {
		
		SerenityRest
				.given()
				.contentType("application/json")
				.when()
				.get(baseUrl+"/workflow/ig/engage/work/getallwork");
	}

	@Step
	public int getWork() {
		return SerenityRest.then().extract().statusCode();
	}

	@Step
	public void requestAddWork() {
		
		Map<String,Object> addMap=new HashMap<String, Object>();
		
		addMap.put("assignedToUserId", 0);
		addMap.put("assignedToUserName", "String");
		addMap.put("currentFlowId", 0);
		addMap.put("currentFlowName", "String");
		addMap.put("endDate", new Date());
		addMap.put("name", "SerenityTest");
		addMap.put("startDate", new Date());
		addMap.put("workDesc", "SerenityTest");
		addMap.put("workStatus", "String");
		
		
		workId=
		SerenityRest
		.given()
		.contentType("application/json").content(addMap).log().body()
		.when()
		.post(baseUrl+"/workflow/ig/engage/work/addwork").then().extract().body().jsonPath().get("workId");
		
	}

	@Step
	public int responseAddWork() {
				
		return SerenityRest.then().extract().statusCode();
	}

	@Step
	public void requestDeleteWork() {
		
		SerenityRest
		.given()
		.contentType("application/json")
		.when()
		.delete(baseUrl+"/workflow/ig/engage/work/deletework?workId="+workId);
	}

	@Step
	public int responseDeleteWork() {
		return SerenityRest.then().extract().statusCode();
	}

	@Step
	public void requestUpdateWork() {
		Map<String,Object> addMap=new HashMap<String, Object>();
		addMap.put("workId", workId);
		addMap.put("assignedToUserId", 0);
		addMap.put("assignedToUserName", "String");
		addMap.put("currentFlowId", 0);
		addMap.put("currentFlowName", "String");
		addMap.put("endDate", new Date());
		addMap.put("name", "SerenityTestUpdate");
		addMap.put("startDate", new Date());
		addMap.put("workDesc", "SerenityTestUpdate");
		addMap.put("workStatus", "String");
		
		
		workId=
				SerenityRest
				.given()
				.contentType("application/json").content(addMap).log().body()
				.when()
				.put(baseUrl+"/workflow/ig/engage/work/updatework").then().extract().body().jsonPath().get("workId");
		
	}

	@Step
	public int responseUpdateWork() {
		return SerenityRest.then().extract().statusCode();
	}
}
