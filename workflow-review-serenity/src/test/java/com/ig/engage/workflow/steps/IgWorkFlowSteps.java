package com.ig.engage.workflow.steps;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class IgWorkFlowSteps {
	
	
	String baseUrl;
	

	public IgWorkFlowSteps() {
		super();
		// TODO Auto-generated constructor stub
		this.baseUrl = System.getProperty("running.app.baseurl");
	}

	

	@Step
	public void requestWorkFlow(long userId){
		SerenityRest
		.given()
		.contentType("application/json")
		.when()
		.get(baseUrl+"/workflow/ig/engage/getworkdatabyuserid?userId="+userId);
	}
	
	@Step
	public int responseWorkFlow(){
		return SerenityRest.then().extract().statusCode();
	}
	
	
}
