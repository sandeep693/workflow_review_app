package com.ig.engage.workflow.definitions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import net.thucydides.core.annotations.Steps;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.ig.engage.workflow.steps.FlowSteps;

public class FlowDefinitions {
	
	@Steps
	FlowSteps flowSteps;
	
	@When("I call getAll FLOW from WorkFLow api")
	public void  callGetFlowApi(){
		flowSteps.requestGetFlow();
	}
	
	@Then("I should getAll FLOW response status code $expectedStatusCode")
	public void  responseGetFlowApi(int expectedStatusCode){
		assertThat("Wrong response status code", flowSteps.responseGetFlow() ,equalTo(expectedStatusCode));
	}

	@When("I call ADD FLOW from WorkFLow api")
	public void callAddFlowApi(){
		flowSteps.requestAddFlow();
		
	}
	@Then("I should ADD FLOW response status code $expectedStatusCode")
	public void responseAddFlowApi(int expectedStatusCode){
		assertThat("Wrong response status code", flowSteps.responseAddFlow() ,equalTo(expectedStatusCode));
	}
	
	@When("I call UPDATE FLOW from WorkFLow api")
	public void callUpdateFlowApi(){
		flowSteps.requestUpdateFlow();
	}

	@Then("I should UPDATE FLOW response status code $expectedStatusCode")
	public void responseUpdateFlowApi(int expectedStatusCode){
		assertThat("Wrong response status code", flowSteps.responseUpdateFlow() ,equalTo(expectedStatusCode));
	}
	
	@When("I call DELETE FLOW from WorkFLow api")
	public void callDeleteFlowApi(){
		flowSteps.requestDeleteFlow();
	}
	
	@Then("I should DELETE FLOW response status code $expectedStatusCode")
	public void responseDeleteFlowApi(int expectedStatusCode){
		assertThat("Wrong response status code", flowSteps.responseDeleteFlow() ,equalTo(expectedStatusCode));
	}
	
	
	
}
