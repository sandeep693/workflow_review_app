package com.ig.engage.workflow.definitions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import net.thucydides.core.annotations.Steps;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.ig.engage.workflow.steps.IgWorkFlowSteps;

public class IgWorkFlowDefinitions {

	
	@Steps
	IgWorkFlowSteps igWorkFlowSteps;
	
	
	@When("I call get WORKFLOW All api for user $userId")
	public void callForGetWorkFlow(long userId){
		igWorkFlowSteps.requestWorkFlow(userId);
	}
	
	@Then("I should get WORKFLOW response status code $expectedStatusCode")
	public void responseGetWorkFlow(int expectedStatusCode){
		assertThat("Wrong response status code", igWorkFlowSteps.responseWorkFlow() ,equalTo(expectedStatusCode));
	}
}
