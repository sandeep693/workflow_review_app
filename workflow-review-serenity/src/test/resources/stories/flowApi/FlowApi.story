Meta: Get all workflow test

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: Get All FLOW form Workflow Application
When I call getAll FLOW from WorkFLow api
Then I should getAll FLOW response status code 200

Scenario: ADD FLOW form Workflow Application
When I call ADD FLOW from WorkFLow api
Then I should ADD FLOW response status code 200

Scenario: UPDATE FLOW form Workflow Application
When I call UPDATE FLOW from WorkFLow api
Then I should UPDATE FLOW response status code 200

Scenario: DELETE FLOW form Workflow Application
When I call DELETE FLOW from WorkFLow api
Then I should DELETE FLOW response status code 200