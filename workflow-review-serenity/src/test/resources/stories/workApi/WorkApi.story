Meta: Get all workflow test

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: Get All WORK form Workflow Application
When I call getAll WORK from WorkFlow All api
Then I should getAll WORK response status code 200

Scenario: Add Work form Workflow Application
When I call ADD WORKFLOW WorkFlow All api
Then I should ADD WORK response status code 200

Scenario: Update Work form Workflow Application
When I call UPDATE WorkFlow All api
Then I should UPDATE response status code 200

Scenario: Delete Work form Workflow Application
When I call DELETE WorkFlow All api
Then I should DELETE response status code 200
